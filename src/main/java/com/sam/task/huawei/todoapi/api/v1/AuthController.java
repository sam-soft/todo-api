package com.sam.task.huawei.todoapi.api.v1;

import com.sam.task.huawei.todoapi.api.v1.command.LoginRequest;
import com.sam.task.huawei.todoapi.api.v1.dto.LoginResponse;
import com.sam.task.huawei.todoapi.security.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/auth")
public class AuthController {

    private final AuthenticationService authenticationService;

    @PostMapping("/user")
    public LoginResponse login(@Valid @RequestBody LoginRequest loginRequest) {
        return authenticationService.authenticate(loginRequest);
    }
}
