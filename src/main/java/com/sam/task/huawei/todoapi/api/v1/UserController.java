package com.sam.task.huawei.todoapi.api.v1;

import com.sam.task.huawei.todoapi.api.v1.command.RegistrationCommand;
import com.sam.task.huawei.todoapi.api.v1.converter.UserDTOConverter;
import com.sam.task.huawei.todoapi.api.v1.dto.UserDTO;
import com.sam.task.huawei.todoapi.domain.User;
import com.sam.task.huawei.todoapi.domain.UserPermission;
import com.sam.task.huawei.todoapi.exception.UserAlreadyExistException;
import com.sam.task.huawei.todoapi.repository.UserRepository;
import com.sam.task.huawei.todoapi.security.CurrentPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/users")
public class UserController {

    private final UserDTOConverter userDTOConverter;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final CurrentPrincipal currentPrincipal;

    @PostMapping("/register")
    public UserDTO register(@Valid @RequestBody RegistrationCommand command) {

        userRepository.findByEmail(command.getEmail())
                .ifPresent(x -> {
                    throw new UserAlreadyExistException();
                });

        User user = prepareUser(command);
        User persistUser = userRepository.save(user);
        return userDTOConverter.convert(persistUser);
    }

    @PreAuthorize("hasRole('REGULAR_USER_PERMISSION')")
    @GetMapping("/me")
    public UserDTO me(){
        User user = userRepository.getById(currentPrincipal.getUserId());
        return userDTOConverter.convert(user);
    }

    @PreAuthorize("hasRole('ROLE_REGULAR_USER')")
    @GetMapping("/me2")
    public String mex(){
        User user = userRepository.getById(currentPrincipal.getUserId());
        return "Me2";
    }




    private User prepareUser(RegistrationCommand command) {
       return  User.builder()
                .userName(command.getUsername())
                .password(bCryptPasswordEncoder.encode(command.getPassword()))
                .email(command.getEmail())
                .permissions(Arrays.asList(UserPermission.REGULAR_USER_PERMISSION))
                .build();
    }

}
