package com.sam.task.huawei.todoapi.api.v1.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class LoginRequest {
    @Email
    private String email;

    @Length(min = 5)
    private String password;
}
