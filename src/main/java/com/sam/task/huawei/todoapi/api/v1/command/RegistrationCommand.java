package com.sam.task.huawei.todoapi.api.v1.command;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegistrationCommand {

    @NotNull
    private String username;

    @Email
    private String email;

    @NotNull
    @Size(min = 6, max = 30)
    private String password;
}
