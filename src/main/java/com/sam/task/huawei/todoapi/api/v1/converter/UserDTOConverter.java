package com.sam.task.huawei.todoapi.api.v1.converter;

import com.sam.task.huawei.todoapi.api.v1.dto.UserDTO;
import com.sam.task.huawei.todoapi.domain.User;
import org.springframework.stereotype.Component;

@Component
public class UserDTOConverter extends BaseConverter <User, UserDTO> {

    @Override
    public UserDTO convert(User source) {
        return UserDTO.builder()
                .username(source.getUserName())
                .email(source.getEmail())
                .permissions(source.getPermissions())
                .build();
    }
}
