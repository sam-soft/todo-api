package com.sam.task.huawei.todoapi.api.v1.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class LoginResponse {
    private String token;
    private Integer timeoutInMinutes;
}
