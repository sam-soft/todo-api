package com.sam.task.huawei.todoapi.api.v1.dto;

import com.sam.task.huawei.todoapi.domain.UserPermission;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserDTO {
    private String username;
    private String email;
    private List<UserPermission> permissions;
}
