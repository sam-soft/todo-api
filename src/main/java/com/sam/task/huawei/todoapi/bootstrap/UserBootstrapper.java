package com.sam.task.huawei.todoapi.bootstrap;

import com.sam.task.huawei.todoapi.domain.User;
import com.sam.task.huawei.todoapi.domain.UserPermission;
import com.sam.task.huawei.todoapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Component
public class UserBootstrapper implements CommandLineRunner {

    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
        userRepository.saveAll(initializeUsers());
    }

    private List<User> initializeUsers() {
        return Arrays.asList(
                new User("username1", "sam1@sam.com", passwordEncoder.encode("password"), Arrays.asList(UserPermission.REGULAR_USER_PERMISSION), true),
                new User("username2", "sam2@sam.com", passwordEncoder.encode("password"), Arrays.asList(UserPermission.REGULAR_USER_PERMISSION), true),
                new User("username3", "sam3@sam.com", passwordEncoder.encode("password"), Arrays.asList(UserPermission.REGULAR_USER_PERMISSION), true)
        );
    }
}