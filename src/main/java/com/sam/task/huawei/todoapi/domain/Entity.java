package com.sam.task.huawei.todoapi.domain;

import lombok.Getter;
import org.apache.tomcat.jni.Local;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class Entity {

    @Getter
    @Id
    @Column(nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
    private Long id;

    @Getter
    @Column(updatable = false, nullable = false)
    private LocalDateTime createDate;

    @Getter
    @Column(nullable = false)
    private LocalDateTime updateDate;

    @Column(nullable = false)
    private Boolean deleted = false;

    @Version
    private Long version;

    public void markAsDeleted(){
        deleted = true;
    }

    public void unmarkDeleted() {
        deleted = false;
    }

    @PrePersist
    private void prePersist(){
        createDate = updateDate = LocalDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        updateDate = LocalDateTime.now();
    }

}
