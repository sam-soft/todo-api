package com.sam.task.huawei.todoapi.domain;

import lombok.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.List;

@Builder
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Data
@Entity
@SequenceGenerator(name = "SEQ",sequenceName = "user_id_seq",allocationSize = 1)
public class User extends com.sam.task.huawei.todoapi.domain.Entity {

    @Column(nullable = false)
    private String userName;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @ElementCollection(fetch = FetchType.LAZY)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "user_permission")
    @Column(name = "permission")
    private List<UserPermission> permissions;

    @Builder.Default
    @Column(nullable = false)
    private boolean isEnabled = true;

}
