package com.sam.task.huawei.todoapi.exception;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException (String message) {
        super(message);
    }

    public UserNotFoundException() {
        
    }

}
