package com.sam.task.huawei.todoapi.repository;

import com.sam.task.huawei.todoapi.domain.User;
import com.sam.task.huawei.todoapi.exception.UserNotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Optional<User> findById(Long id);

    default User getById(Long id) {
        return findById(id)
                .orElseThrow(UserNotFoundException::new);
    }

}
