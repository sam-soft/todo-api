package com.sam.task.huawei.todoapi.security;

import com.sam.task.huawei.todoapi.domain.User;

public interface CurrentPrincipal {
    Long getUserId();
}
