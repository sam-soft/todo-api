package com.sam.task.huawei.todoapi.security.service;

import com.sam.task.huawei.todoapi.api.v1.command.LoginRequest;
import com.sam.task.huawei.todoapi.api.v1.dto.LoginResponse;

public interface AuthenticationService {
    LoginResponse  authenticate(LoginRequest loginRequest);
}
