package com.sam.task.huawei.todoapi.security.service;

import com.sam.task.huawei.todoapi.api.v1.command.LoginRequest;
import com.sam.task.huawei.todoapi.api.v1.dto.LoginResponse;
import com.sam.task.huawei.todoapi.security.token.JwtTokenProvider;
import com.sam.task.huawei.todoapi.security.token.JwtUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    @Value("${security.jwt.expireTime}")
    private Integer expireTime;

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @PostConstruct
    public void init() {
        expireTime = expireTime/60000; //to minute
    }

    @Override
    public LoginResponse authenticate(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        String token = jwtTokenProvider.generate((JwtUserDetails) authentication.getPrincipal());
        return new LoginResponse(token, expireTime);
    }
}
