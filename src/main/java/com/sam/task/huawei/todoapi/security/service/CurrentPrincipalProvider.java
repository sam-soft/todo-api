package com.sam.task.huawei.todoapi.security.service;

import com.sam.task.huawei.todoapi.security.CurrentPrincipal;
import com.sam.task.huawei.todoapi.security.token.JwtUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CurrentPrincipalProvider implements CurrentPrincipal {

    @Override
    public Long getUserId() {
        return getUserDetails().getId();
    }

    private JwtUserDetails getUserDetails() {
        return (JwtUserDetails) getAuthenticationToken().getPrincipal();
    }

    private UsernamePasswordAuthenticationToken getAuthenticationToken() {
        return (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
    }
}
