package com.sam.task.huawei.todoapi.security.service;

import com.sam.task.huawei.todoapi.domain.User;
import com.sam.task.huawei.todoapi.exception.UserNotFoundException;
import com.sam.task.huawei.todoapi.repository.UserRepository;
import com.sam.task.huawei.todoapi.security.token.JwtUserDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return loadUserByEmail(email);
    }

    @Transactional
    public UserDetails loadUserByEmail(String email) {
        Optional<User> user = userRepository.findByEmail(email);
        if (!user.isPresent()) {
            log.error("user not found for this email : {}", email);
            throw new AccessDeniedException("user not found for this email", new UserNotFoundException());
        }
        return user.map(JwtUserDetails::create).get();
    }
}
