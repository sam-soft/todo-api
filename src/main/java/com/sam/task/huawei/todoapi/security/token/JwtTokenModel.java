package com.sam.task.huawei.todoapi.security.token;

import com.sam.task.huawei.todoapi.domain.UserPermission;
import lombok.Builder;
import lombok.Getter;

import java.util.List;


@Getter
@Builder
public class JwtTokenModel {
    private String email;
    private List<UserPermission> permission;
}
