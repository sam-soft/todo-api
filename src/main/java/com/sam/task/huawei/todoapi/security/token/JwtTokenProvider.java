package com.sam.task.huawei.todoapi.security.token;

import java.util.Optional;

public interface JwtTokenProvider {
    String generate(JwtUserDetails userDetails);
    Optional<JwtTokenModel> parse(String token);
}
