package com.sam.task.huawei.todoapi.security.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Slf4j
@Service
public class JwtTokenProviderImpl implements JwtTokenProvider {
    @Value("${security.jwt.secretKey}")
    private String secretKey;

    @Value("${security.jwt.expireTime}")
    private Integer expireTime;

    @Override
    public String generate(JwtUserDetails userDetails) {

        return  JWT.create()
                .withSubject(userDetails.getEmail())
                .withIssuedAt(new Date(System.currentTimeMillis() ))
                .withExpiresAt(new Date(System.currentTimeMillis() + expireTime))
                .sign(Algorithm.HMAC512(secretKey));
    }

    @Override
    public Optional<JwtTokenModel> parse(String token) {
        try{
            DecodedJWT jwt = JWT.require(Algorithm.HMAC512(secretKey)).build().verify(token);

            JwtTokenModel jwtTokenModel = JwtTokenModel.builder()
                    .email(jwt.getSubject())
                    .build();

            return Optional.of(jwtTokenModel);
        }catch (JWTVerificationException e ){
            log.error(e.getMessage(), e);
            return Optional.empty();
        }
    }
}
