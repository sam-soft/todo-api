package com.sam.task.huawei.todoapi.api.v1;

import com.sam.task.huawei.todoapi.api.v1.command.LoginRequest;
import com.sam.task.huawei.todoapi.api.v1.dto.LoginResponse;
import com.sam.task.huawei.todoapi.security.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthControllerTest extends AbstractRestControllerTest {

    @Mock
    AuthenticationService authService;

    @InjectMocks
    AuthController authController;

    MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(authController).build();
    }

    @Test
    public void loginTest() throws Exception {

        //GIVEN
        LoginRequest loginRequest = new LoginRequest("test@test.com", "password");
        String mockToken = "skgemlwenglqwnglknqwlgqw.gqwgqwgw3g.w3ghwW";
        LoginResponse authenticationResonse = new LoginResponse(mockToken, 10);
        when(authService.authenticate(any())).thenReturn(authenticationResonse);

        //WHEN
        mockMvc.perform(
                post("/v1/auth/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(loginRequest))
        )
                //THEN
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token", equalTo(mockToken)))
                .andExpect(jsonPath("$.timeoutInMinutes", equalTo(authenticationResonse.getTimeoutInMinutes())));
    }

}