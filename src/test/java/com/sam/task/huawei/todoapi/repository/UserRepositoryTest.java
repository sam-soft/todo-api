package com.sam.task.huawei.todoapi.repository;

import com.sam.task.huawei.todoapi.domain.User;
import com.sam.task.huawei.todoapi.domain.UserPermission;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TestEntityManager entityManager;

    @Test
    public void findAll() {
        //given
        List<User> givenUsers =  Arrays.asList(
                new User("username1", "email1@email.com", "password", Arrays.asList(UserPermission.REGULAR_USER_PERMISSION), true),
                new User("username2", "email2@email.com", "password", Arrays.asList(UserPermission.REGULAR_USER_PERMISSION), true),
                new User("username3", "email3@email.com", "password", Arrays.asList(UserPermission.REGULAR_USER_PERMISSION), true)
        );

        givenUsers.forEach(entityManager::persist);

        //when
        List<User> resultUsers = userRepository.findAll();

        //then
        assertEquals("record count does not match",givenUsers.size(), resultUsers.size());
    }

    @Test
    public void findByEmail() {
        //given
        User givenUser =  new User("username1", "email1@email.com", "password", Arrays.asList(UserPermission.REGULAR_USER_PERMISSION), true);
        entityManager.persist(givenUser);

        //when
        Optional<User> byEmail = userRepository.findByEmail(givenUser.getEmail());

        //then
        assertTrue("the user which is getting by email must be exist", byEmail.isPresent());
    }

}