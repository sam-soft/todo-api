package com.sam.task.huawei.todoapi.security.service;

import com.sam.task.huawei.todoapi.domain.User;
import com.sam.task.huawei.todoapi.domain.UserPermission;
import com.sam.task.huawei.todoapi.repository.UserRepository;
import com.sam.task.huawei.todoapi.security.token.JwtUserDetails;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class JwtUserDetailsServiceTest {

    JwtUserDetailsService jwtUserDetailsService;

    @Mock
    UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        jwtUserDetailsService = new JwtUserDetailsService(userRepository);
    }

    @Test
    public void loadUserByUsername() {
        //given
        String email = "sam@sam.com";
        Optional<User> mockUser = Optional.of(new User("username", email, "password", Arrays.asList(UserPermission.REGULAR_USER_PERMISSION),true));
        when(userRepository.findByEmail(anyString())).thenReturn(mockUser);

        //when
        JwtUserDetails jwrUserDetails = (JwtUserDetails) jwtUserDetailsService.loadUserByEmail(email);

        //then
        assertEquals("does not match email fields", jwrUserDetails.getEmail(), email);
        assertEquals("does not match password fields", jwrUserDetails.getPassword(), "password");
        verify(userRepository, times(1)).findByEmail(anyString());
        verify(userRepository, never()).findById(anyLong());
    }
}