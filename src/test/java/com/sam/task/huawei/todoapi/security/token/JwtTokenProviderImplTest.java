package com.sam.task.huawei.todoapi.security.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(value={"classpath:application.yml"})
@SpringBootTest(classes = {JwtTokenProviderImpl.class})
public class JwtTokenProviderImplTest {


    @Value("${security.jwt.secretKey}")
    String secretKey;

    @Value("${security.jwt.expireTime}")
    Integer expireTime;

    @Autowired
    JwtTokenProviderImpl jwtTokenProvider;


    @Test
    public void generate() {

        //given
        String email = "sam@sam.com";
        JwtUserDetails userDetails = JwtUserDetails.builder()
                .id(1l)
                .email(email)
                .authorities(null)
                .enabled(true)
                .build();

        //when
        String generatedToken = jwtTokenProvider.generate(userDetails);

        //then
        DecodedJWT jwt = JWT.require(Algorithm.HMAC512(secretKey)).build().verify(generatedToken);
        assertEquals("decoded value must be equals generated", email, jwt.getSubject());

    }

    @Test
    public void parse() {

        //given
        String email = "sam@sam.com";
        String token = JWT.create()
                .withSubject(email)
                .withIssuedAt(new Date(System.currentTimeMillis()))
                .withExpiresAt(new Date(System.currentTimeMillis() + expireTime))
                .sign(Algorithm.HMAC512(secretKey));
        //when
        Optional<JwtTokenModel> parsedTokenModel = jwtTokenProvider.parse(token);

        //then
        assertTrue("parsed token model must be present", parsedTokenModel.isPresent());
        assertEquals("parsed token email must be equal given", email, parsedTokenModel.get().getEmail());
    }


    @Test
    public void expiredParse() throws Exception {

        //given
        Integer expireTime = 1;
        String email = "sam@sam.com";
        String token = JWT.create()
                .withSubject(email)
                .withIssuedAt(new Date(System.currentTimeMillis()))
                .withExpiresAt(new Date(System.currentTimeMillis() + expireTime))
                .sign(Algorithm.HMAC512(secretKey));


        //when
        Thread.sleep(1000);
        Optional<JwtTokenModel> parsedTokenModel = jwtTokenProvider.parse(token);

        //then
        assertFalse("expired token model must be empty", parsedTokenModel.isPresent());
    }
}